fom xml.dom.minidom import Element
from appium import webdriver
from appium.options.ios import XCUITestOptions
from appium.webdriver.common.appiumby import AppiumBy
import time
import json
import pandas as pd
class MobileCrawl():
    def __init__(self):
        self.des_cap = {
            "uuid":"emulator-5554",
            "platformName": "android",
            "appium:platformVersion": "12",
            "appium:deviceName": "kane",
            "appium:automationName": "UIAutomator2",
            "appium:appPackage": "com.deliverynow","appium:appActivity": "foody.vn.deliverynow.MainActivity"
            }
        # Start the app
        self.driver = webdriver.Remote("http://localhost:4723/wd/hub",self.des_cap)
        self.driver.implicitly_wait(60)
        self.wait_time = 20
        self.name_list = []
        self.comment_list = []
        self.time_list = []
        self.check_stop = 0
    def Choose_loccation(self,enter_location_xpath,map_xpath,bypass_location_xpath):
        location = self.driver.find_element(by=AppiumBy.XPATH,value = enter_location_xpath)
        location.click()
        self.driver.implicitly_wait(self.wait_time)
        map_location = self.driver.find_element(by=AppiumBy.XPATH, value = map_xpath)
        map_location.click()
        self.driver.implicitly_wait(self.wait_time)
        bypass_location = self.driver.find_element(by=AppiumBy.XPATH,value=bypass_location_xpath)
        bypass_location.click()
        self.driver.implicitly_wait(self.wait_time)
    def bypass_advertising(self,advertising_xpath):
        advertising = self.driver.find_element(by=AppiumBy.XPATH,value=advertising_xpath)
        advertising.click()
        self.driver.implicitly_wait(self.wait_time)
    def search_food_stores(self,food_store_search_xpath,food_category_xapth):
        food_store_search = self.driver.find_element(by=AppiumBy.XPATH,value =food_store_search_xpath)
        food_store_search.click()
        self.driver.implicitly_wait(self.wait_time)
        food_category = self.driver.find_element(by=AppiumBy.XPATH,value =food_category_xapth)
        food_category.click()
        self.driver.implicitly_wait(self.wait_time)
    def search_eat_good_store(self):
        search_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText'
        search_xpath_click = self.driver.find_element(by=AppiumBy.XPATH, value=search_xpath)
        search_xpath_click.click()
        self.driver.implicitly_wait(self.wait_time)

        search_value = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText'
        search_value_click = self.driver.find_element(by=AppiumBy.XPATH, value=search_value)
        search_value_click.send_keys('eat good')
        self.driver.implicitly_wait(self.wait_time)

        search_result = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView'
        search_result_click = self.driver.find_element(by=AppiumBy.XPATH, value=search_result)
        search_result_click.click()
        self.driver.implicitly_wait(self.wait_time)

    def get_food_store(self,food_store_xpath):
        food_store = self.driver.find_element(by=AppiumBy.XPATH,value =food_store_xpath)
        food_store.click()
        self.driver.implicitly_wait(self.wait_time)
    def click_all_reviews(self,all_reviews_xpath):
        all_reviews_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.view.ViewGroup[6]'
        all_reviews = self.driver.find_element(by=AppiumBy.XPATH,value =all_reviews_xpath)
        all_reviews.click()
        self.driver.implicitly_wait(self.wait_time)

        # click view coment
        # comment_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView[1]'
        # all_comment = self.driver.find_element(by=AppiumBy.XPATH,value =comment_xpath)
        # all_comment.click()
        # self.driver.implicitly_wait(self.wait_time)

        five_star_xpath = ''
        five_star_comment = self.driver.find_element(by=AppiumBy.XPATH,value =five_star_xpath)
        five_star_comment.click()
        self.driver.implicitly_wait(self.wait_time)

    def check_time(self,time_info):
        if time_info in self.time_list:
            print('Remove')
            del self.time_list[-1]
            del self.name_list[-1]
            del self.comment_list[-1]
            self.check_stop+=1
        else:
            self.check_stop = 0 
         

    def get_reviews(self,reviews_xpath,all_reviews_infor_xpath):
        try:
            name_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.TextView'
            name = self.driver.find_element(by=AppiumBy.XPATH,value =name_xpath)
            print(name.text)
            self.name_list.append(str(name.text))
            comment_info_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView[1]'
            comment = self.driver.find_element(by=AppiumBy.XPATH,value =comment_info_xpath)
            print(comment.text)
            self.comment_list.append(str(comment.text))
            time_xpath = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView[2]'
            time_info = self.driver.find_element(by=AppiumBy.XPATH,value =time_xpath)
            print(time_info.text)
            self.check_time(str(time_info.text))
            self.time_list.append(str(time_info.text))
            time.sleep(1)
        except:
            print('Error')
   
    def scrolling(self):
        self.driver.swipe(486, 1439, 460, 800, 3000)
    def close_driver(self):
        self.driver.quit()
    def get_need_infor(self):
        f = open('xpaths.json')
        all_xpath = json.load(f)
        print('location')
        self.Choose_loccation(all_xpath['enter_location_xpath'],all_xpath['map_xpath'],all_xpath['bypass_location_xpath'])
        print('advertersing')
        self.bypass_advertising(all_xpath['advertising_xpath'])
        print('search food store')
        self.search_eat_good_store()
        # self.search_food_stores(all_xpath['food_store_search_xpath'],all_xpath['food_category_xapth'])
        print('click food store')
        self.get_food_store(all_xpath['food_store_xpath'])
        print('Get reviews')
        self.click_all_reviews(all_xpath['all_reviews_xpath'])
        while True:
            self.get_reviews(all_xpath['reviews_xpath'],all_xpath['all_reviews_infor_xpath'])
            self.scrolling()
            print('--- Check Stop ---')
            print(self.check_stop)
            print('-------------------')
            if self.check_stop >= 3:
                break   
        data = {
            'names': self.name_list,
            'comments': self.comment_list,
            'times': self.time_list,
        }
        
        result = pd.DataFrame(data)
        result.to_csv('eat_good.csv')
        print('Stop all')
        time.sleep(5)
        self.close_driver()

data = MobileCrawl()
data.get_need_infor()


    
